package ecosysteme;

public class Proie extends Animal{
  private static int energie_max = 16;


  public Proie(int x, int y){
    super(x, y, energie_max);
  }

  @Override
  public void Manger(int quantite){
    for(int i = 0; i < quantite; i++){
      if (getEnergie() < energie_max){
        setEnergie(getEnergie() + 1);
      }
    }
  }

  @Override
  public String toString() {
    String symbole = " z" + getEnergie();
    return symbole;
  }
  @Override
  public int getX() {
    // TODO Auto-generated method stub
    return super.getX();
  }
  @Override
  public int getY() {
    // TODO Auto-generated method stub
    return super.getY();
  }

  public void Mourir(){
    setEnergie(0);
  }

  @Override
  public void Bouger(Ecosysteme ecosysteme){
    int random = (int)(Math.random()*4);
    int nx = getX();
    int ny = getY();
    if (random == 1){
      nx = nx + 1;
    }else if(random == 2){
      nx = nx - 1;
    }else if(random == 3){
      ny = ny + 1;
    }else if(random == 0){
      ny = ny - 1;
    }
    if(nx > ecosysteme.grille_.length-1){
      nx = 0;
    }else if(nx < 0){
      nx = ecosysteme.grille_.length-1;
    }
    if(ny > ecosysteme.grille_[1].length-1){
      ny = 0;
    }else if(ny < 0){
      ny = ecosysteme.grille_[1].length-1;
    }
    if(ecosysteme.grille_[nx][ny] == null){
      setX(nx);
      setY(ny);
    }else if(ecosysteme.grille_[nx][ny] instanceof Nourriture){
      setX(nx);
      setY(ny);
      Nourriture nourriture = (Nourriture)ecosysteme.grille_[nx][ny];
      Manger(nourriture.getCalories());
      ecosysteme.grille_[nx][ny] = null;
    }
    // else if(ecosysteme.grille_[nx][ny] instanceof Predateur){
      
    // }
  }


}
