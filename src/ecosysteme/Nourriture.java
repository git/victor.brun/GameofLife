package ecosysteme;

public class Nourriture extends Cellule{
  private int calories_;
  

  public Nourriture(int x_, int y_, int calories){
    super(x_,y_);
    calories_ = calories;
  }


  @Override
  public String toString() {
    String symbole = "";
    if (calories_ == 1){
      symbole += " . ";
    }else if(calories_ == 2){
      symbole += " v ";
    }else{
      symbole += " w ";
    }
    return symbole;
  }
  public int getCalories() {
    return calories_;
  }
  @Override
  public int getX() {
    // TODO Auto-generated method stub
    return super.getX();
  }
  @Override
  public int getY() {
    // TODO Auto-generated method stub
    return super.getY();
  }


}
