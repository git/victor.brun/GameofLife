package ecosysteme;

public class Cellule {
  protected int x_;
  protected int y_;

  public Cellule(int x, int y){
    x_ = x;
    y_ = y;
  }

  @Override
  public String toString() {
    return "?";
  }
  // getter
  public int getX() {
    return x_;
  }
  public int getY() {
    return y_;
  }
  // setter
  public void setX(int x_) {
    this.x_ = x_;
  }
  public void setY(int y_) {
    this.y_ = y_;
  }


}
