package ecosysteme;

public class Ecosysteme {
  public Cellule[][] grille_; 
  private Nourriture[] nourriture_;
  private Animal[] animaux_;
  private int hauteur_;
  private int largeur_;
  private int nbAnimal_;


  public Ecosysteme(int hauteur, int largeur){
    hauteur_ = hauteur;
    largeur_ = largeur;
    grille_ = new Cellule[hauteur_][largeur_];
  }


  public static void main(String[] args) {
    Ecosysteme eco_ = new Ecosysteme(5,5);
    eco_.Initialiser(10, 5, 5);
  }
  private void remplirGrille(){
    viderGrille();
    for (int i = 0; i < nourriture_.length; i++){
        grille_[nourriture_[i].getX()][nourriture_[i].getY()] = nourriture_[i];
    }
    for (int i = 0; i < nbAnimal_; i++){
      grille_[animaux_[i].getX()][animaux_[i].getY()] = animaux_[i];
    }
  }

  private void Afficher(){
    for (int i = 0; i < grille_.length; i++){
      for (int j = 0; j < grille_[i].length; j++){
        if(grille_[i][j] != null){
          System.out.print(grille_[i][j]);
        }else{
          System.out.print("   ");
        }
      }
      System.out.println();
    }
  }

  private void creerNourriture(int quantite){
    nourriture_ = new Nourriture[quantite];
    for (int i = 0; i < quantite; i++){
      int x = (int)(Math.random()*grille_.length);
      int y = (int)(Math.random()*grille_[1].length);
      int calories = (int)(Math.random()*3)+1;
      Nourriture nourriture = new Nourriture(x,y,calories);
      do{
        x = (int)(Math.random()*grille_.length);
        y = (int)(Math.random()*grille_[1].length);
      } while (grille_[x][y] != null);
        nourriture_[i] = nourriture;
    }
    // for (int i = 0; i< quantite; i++){
    //   System.out.print(nourriture_[i]);
    //   System.out.print(nourriture_[i].getX());
    //   System.out.print(nourriture_[i].getY());
    // }
    System.out.println();
  }





  public void creerAnimal(int nbProies, int nbPredateur){
    nbAnimal_ = nbPredateur + nbProies;
    animaux_ = new Animal[nbAnimal_];
    for (int i = 0; i < nbProies; i++){
      int x = (int)(Math.random()*grille_.length);
      int y = (int)(Math.random()*grille_[1].length);
      Proie proie = new Proie(x,y);
      do{
        x = (int)(Math.random()*grille_.length);
        y = (int)(Math.random()*grille_[1].length);
      } while (grille_[x][y] != null);
        animaux_[i] = proie;
    }
    for (int i = nbProies; i < nbAnimal_; i++){
      int x = (int)(Math.random()*grille_.length);
      int y = (int)(Math.random()*grille_[1].length);
      Predateur predateur = new Predateur(x,y);
      do{
        x = (int)(Math.random()*grille_.length);
        y = (int)(Math.random()*grille_[1].length);
      } while (grille_[x][y] != null);
        animaux_[i] = predateur;
    }
    // remplirGrille();
    // for (int i = 0; i< quantite; i++){
    //   System.out.print(animaux_[i]);
    // }
    // System.out.println();
  }

  public int getHauteur() {
    return hauteur_;
  }
  public int getLargeur() {
    return largeur_;
  }

  public void viderGrille(){
    for(int i = 0; i < grille_.length; i++){
      for(int j = 0; j < grille_[1].length; j++){
        grille_[i][j] = null;
      }
    }
  }
  public void Initialiser(int nourriture, int proie, int predateur){
    creerAnimal(proie, predateur);
    creerNourriture(nourriture);
    remplirGrille();
    Afficher();
    Simuler();
  }

  public void Simuler(){
    while(nbAnimal_ > 0){
      try{
        Thread.sleep(1000); 
        Actualiser();

      }catch (Exception e){

      } 
    }
  }


  public void Actualiser(){
      for(int i = 0; i < animaux_.length; i++){
        animaux_[i].Deperit();
        animaux_[i].Bouger(this);
      }
      int i = 0;
      while (i < nbAnimal_){
        if(animaux_[i].getEnergie() <= 0){
          animaux_[i] = animaux_[nbAnimal_-1];
          nbAnimal_--;
        }else{
          i++;
        }
      }
      System.out.println();
      System.out.println();
      System.out.println();
      remplirGrille();
      Afficher();
    }
}
